/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eval;

import image.classifier.Classifier;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;

/**
 * Membuat berkas ARFF dari data latih dan mevalidasi silang data latih tsb.
 * Semua kegiatan memanfaatkan fungsi pada kelas Classifier (tidak seperti
 * UsingWeka yang memiliki fungsi dan kode internal sendiri).
 *
 * Implementasi kelas ini membuat kelas UsingWeka obsolete.
 *
 * Kelas ini dibuat supaya bisa diakses dengan GUI oleh DocImageUI untuk membuat
 * file ARFF dari folder-folder data uji yang dipih. Juga bisa memilih file ARFF
 * yang digunakan dalam klasifikasinya StepsUI.
 *
 * @author albadr.ln
 */
public class ClassifierEval {

    /**
     * Melakukan validasi silang 10-fold dari data latih yang ada.
     * @param trainingFile 
     */
    public static void crossValidation(File[] trainingFile, String pathToSaveInstance) {
        try {
            //Declare classifier
            Classifier cl = new Classifier();

            // Create an empty training set
            Instances trainingSet = cl.setInstances(trainingFile);
            // Print ARFF
            cl.saveARFF(trainingSet, pathToSaveInstance);

            // J48 classifier
            weka.classifiers.Classifier cModel = (weka.classifiers.Classifier) new J48();

            // Test the model
            Evaluation eTest = new Evaluation(trainingSet);
            eTest.crossValidateModel(cModel, trainingSet, 10, new Random(1));

            // Print the result à la Weka explorer:
            String strSummary = eTest.toSummaryString();
            System.out.println(strSummary);
            //System.out.println(eTest.toClassDetailsString());

        } catch (Exception ex) {
            Logger.getLogger(UsingWeka.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Melakukan validasi model J48 dengan data latih dan uji tertentu.
     * @param trainingFile
     * @param testingFile 
     */
    public static void validate(File[] trainingFile, File[] testingFile) {
        try {
            Classifier cl = new Classifier();

            // Create an empty training set
            Instances trainingSet = cl.setInstances(trainingFile);
            // Print ARFF
            //System.out.println(trainingSet);

            // J48 classifier
            weka.classifiers.Classifier cModel = (weka.classifiers.Classifier) new J48();
            cModel.buildClassifier(trainingSet);

            // Add testing data
            Instances testingSet = cl.setInstances(testingFile);

            // Test the model
            Evaluation eTest = new Evaluation(trainingSet);
            eTest.evaluateModel(cModel, testingSet);

            // Print the result à la Weka explorer:
            String strSummary = eTest.toSummaryString();
            System.out.println(strSummary);
            //System.out.println(eTest.toClassDetailsString());
        } catch (Exception ex) {
            Logger.getLogger(UsingWeka.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Mengumpulkan file-file yang tepat berada di bawah folder.
     *
     * @param folder Kumpulan folder
     * @return Kumpulan file yang ada tepat di bawah masing-masing folder
     */
    public static File[] folderToFiles(File[] folder) {
        ArrayList<File> fileList = new ArrayList<>();

        for (File tf : folder) {
            File[] files = tf.listFiles();
            fileList.addAll(Arrays.asList(files));
        }

        return fileList.toArray(new File[0]);
    }

    /**
     * Melakukan validasi silang 10-fold untuk seluruh data uji yang ada, normal
     * dan bold.
     */
    public static void defaultCrossValidation(String pathToSaveInstance) {
        System.out.println("============================== Validasi Silang ===");
        File[] trainingSet = new File[12];
        trainingSet[0] = new File("_raw/mainbody/01/");
        trainingSet[1] = new File("_raw/mainbody/02/");
        trainingSet[2] = new File("_raw/mainbody/03/");
        trainingSet[3] = new File("_raw/mainbody/04/");
        trainingSet[4] = new File("_raw/mainbody/05/");
        trainingSet[5] = new File("_raw/mainbody/06/");
        trainingSet[6] = new File("_raw/mainbody/bold01/");
        trainingSet[7] = new File("_raw/mainbody/bold02/");
        trainingSet[8] = new File("_raw/mainbody/bold03/");
        trainingSet[9] = new File("_raw/mainbody/bold04/");
        trainingSet[10] = new File("_raw/mainbody/bold05/");
        trainingSet[11] = new File("_raw/mainbody/bold06/");
        crossValidation(folderToFiles(trainingSet), pathToSaveInstance);
    }

    /**
     * Melakukan validasi silang dengan setiap font menjadi data uji dan font
     * sisanya menjadi data latih.
     *
     * Dicetak hasil untik setiap font pada data uji.
     */
    public static void defaultCrossValidationFont() {
        String[] fontName = new String[]{"", "Arial", "Arial Unicode", "Microsoft Sans Serif", "Segoe UI", "Tahoma", "Traditional Arabic"};

        System.out.println("============================== Validasi Silang FONT ===");
        for (int fi = 1; fi <= 6; ++fi) {
            System.out.print("===========================" + fontName[fi]);

            // mengeset data latih
            File[] trainingSet = new File[10];
            int j = 0;
            for (int i = 1; i <= 6; ++i) {
                //yang mau di skip (font yg jadi data uji) tulis di sini
                if (i == fi) {
                    continue;
                }
                //masukin data latih
                trainingSet[j * 2] = new File("_raw/mainbody/0" + i + "/");
                trainingSet[j * 2 + 1] = new File("_raw/mainbody/bold0" + i + "/");
                j++;
            }
            //mengeset data uji
            File[] testingSet = new File[2];
            testingSet[0] = new File("_raw/mainbody/0" + fi + "/");
            testingSet[1] = new File("_raw/mainbody/bold0" + fi + "/");

            validate(folderToFiles(trainingSet), folderToFiles(testingSet));
        }
    }

    public static void main(String[] args) {
        ClassifierEval.defaultCrossValidation("data.arff");
        ClassifierEval.defaultCrossValidationFont();
    }
}
