/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eval;

import image.BinaryImageShell;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;

/**
 *
 * @author Nasution
 */
public class GenerateImageData {

    /**
     * Memotong citra yang berisi kumpulan citra huruf-huruf. Citra harus dalam
     * bentuk matriks 4xN. Horizontal 4 untuk bentuk isolasi, awal, tengah dan
     * akhir huruf. Vertikal N untuk setiap huruf.
     *
     * Panggil method ini untuk setiap font yang diinginkan. File akan disimpan
     * pada nama berkas citra font yang berkaitan.
     *
     * @param filepath Letak file citra
     * @param prefixSave Prefix lokasi tempat penyimpanan potongan citra,
     * relatif thd. _raw/mainbody/
     */
    public static void segmentTableRaw(String filepath, String prefixSave) {
        File file = new File(filepath);
        BinaryImageShell image = new BinaryImageShell(file);
        int NHeight = image.getHeight() / 200;

        for (int wd = 0; wd < 4; ++wd) {
            for (int hd = 0; hd < NHeight; ++hd) {
                int top = hd * 200;
                int left = wd * 200;
                BinaryImageShell crop = image.crop(new Rectangle(new Point(left, top), new Dimension(200, 200)));

                String hdPath = hd >= 10 ? "" + hd : "0" + hd;
                String fileName = file.getName().substring(0, 2);
                crop.saveImage("_raw/mainbody/" + prefixSave + fileName + "/" + hdPath + wd);
            }
        }

    }

    /**
     * Memotong citra yang berisi kumpulan citra huruf angka. Citra harus dalam
     * bentuk matriks 10x6. Horizontal 10 untuk masing-masing angka 0-9.
     * Vertikal 6 untuk setiap font.
     *
     * @param filepath Letak file citra
     * @param prefixSave Prefix lokasi tempat penyimpanan potongan citra,
     * relatif thd. _raw/mainbody/
     */
    public static void segmentAngkaRaw(String filepath, String prefixSave) {
        File file = new File(filepath);
        BinaryImageShell image = new BinaryImageShell(file);

        for (int wd = 0; wd < 10; ++wd) {
            for (int hd = 0; hd < 6; ++hd) {
                int top = hd * 200;
                int left = wd * 200;
                BinaryImageShell crop = image.crop(new Rectangle(new Point(left, top), new Dimension(200, 200)));
                crop.saveImage("_raw/mainbody/" + prefixSave + "0" + (hd + 1) + "/AA" + wd);
            }
        }
    }

    /**
     * Memotong citra yang berisi kumpulan citra huruf lamalif. Citra harus
     * dalam bentuk matriks 2x6. Horizontal dua untuk bentuk awal dan akhir
     * lamalif. Vertikal 6 untuk setiap font.
     *
     * @param filepath Letak file citra
     * @param prefixSave Prefix lokasi tempat penyimpanan potongan citra,
     * relatif thd. _raw/mainbody/
     */
    public static void segmentSpecialLamAlif(String filepath, String prefixSave) {
        File file = new File(filepath);
        BinaryImageShell image = new BinaryImageShell(file);

        for (int wd = 0; wd < 2; ++wd) {
            for (int hd = 0; hd < 6; ++hd) {
                int top = hd * 200;
                int left = wd * 200;
                BinaryImageShell crop = image.crop(new Rectangle(new Point(left, top), new Dimension(200, 200)));
                String wds = wd == 0 ? "" : "." + wd;
                crop.saveImage("_raw/mainbody/" + prefixSave + "0" + (hd + 1) + "/S10" + wds);
            }
        }
    }

    /**
     * Membuat citra miring dari citra-citra yang ada pada folder tertentu.
     * Citra dimiringkan dengan sudut acak dari -30 hingga 30 derajat inklusif.
     *
     * @param folderpath Lokasi folder yang berisi citra yang ingin dimiringkan
     * @param prefixSave Lokasi tempat penyimpanan citra hasil
     */
    public static void generateSkewedPage(String folderpath, String prefixSave) {
        File folder = new File(folderpath);
        File[] imageFiles = ClassifierEval.folderToFiles(new File[]{folder});

        for (File imageFile : imageFiles) {
            BinaryImageShell image = new BinaryImageShell(imageFile);
            int angleDegree = -30 + (int) (Math.random() * 61);
            double angleRadian = Math.toRadians(angleDegree);
            image.rotate(angleRadian);
            image.saveImage(prefixSave + "/" + imageFile.getName() + ", r " + angleDegree);
        }
    }

    public static void generateFixedSkewedPage_moment(String skewedImageFolder, String prefixSave) {
        File folder = new File(skewedImageFolder);
        File[] imageFiles = ClassifierEval.folderToFiles(new File[]{folder});

        long before = System.currentTimeMillis();
        for (File imageFile : imageFiles) {
            BinaryImageShell image = new BinaryImageShell(imageFile);
            image.cropEnvelope();
            image.updateImage();

            //skew correction
            System.out.println("Doing estimation on " + imageFile.getName());
            image.skewCorrection();
            image.cropEnvelope();
            image.updateImage();

            //save
            image.saveImage(prefixSave + "/" + imageFile.getName());
        }
        System.out.println("Skew Correction takes " + (System.currentTimeMillis() - before) + " ms");
    }

    public static void generateFixedSkewedPage_triangle(String skewedImageFolder, String prefixSave) {
        File folder = new File(skewedImageFolder);
        File[] imageFiles = ClassifierEval.folderToFiles(new File[]{folder});

        long before = System.currentTimeMillis();
        for (File imageFile : imageFiles) {
            BinaryImageShell image = new BinaryImageShell(imageFile);
            image.cropEnvelope();
            image.updateImage();

            //skew correction
            System.out.println("Doing estimation on " + imageFile.getName());
            image.mySkewCorrection();
            image.cropEnvelope();
            image.updateImage();

            //save
            image.saveImage(prefixSave + "/" + imageFile.getName());
        }
        System.out.println("Skew Correction takes " + (System.currentTimeMillis() - before) + " ms");
    }

    public static void main(String args[]) {
        //Membuat citra huruf tunggal standar
        GenerateImageData.segmentTableRaw("_raw/mainbody/01.png", "");
        GenerateImageData.segmentTableRaw("_raw/mainbody/02.png", "");
        GenerateImageData.segmentTableRaw("_raw/mainbody/03.png", "");
        GenerateImageData.segmentTableRaw("_raw/mainbody/04.png", "");
        GenerateImageData.segmentTableRaw("_raw/mainbody/05.png", "");
        GenerateImageData.segmentTableRaw("_raw/mainbody/06.png", "");
        GenerateImageData.segmentAngkaRaw("_raw/mainbody/angka.png", "");
        GenerateImageData.segmentSpecialLamAlif("_raw/mainbody/lamalif.png", "");

        //Membuat citra huruf tunggal standar bold
        GenerateImageData.segmentTableRaw("_raw/mainbody/01.bold.png", "bold");
        GenerateImageData.segmentTableRaw("_raw/mainbody/02.bold.png", "bold");
        GenerateImageData.segmentTableRaw("_raw/mainbody/03.bold.png", "bold");
        GenerateImageData.segmentTableRaw("_raw/mainbody/04.bold.png", "bold");
        GenerateImageData.segmentTableRaw("_raw/mainbody/05.bold.png", "bold");
        GenerateImageData.segmentTableRaw("_raw/mainbody/06.bold.png", "bold");
        GenerateImageData.segmentAngkaRaw("_raw/mainbody/angka.png", "bold");
        GenerateImageData.segmentSpecialLamAlif("_raw/mainbody/lamalif.png", "bold");

        //Membuat citra dokumen miring
        GenerateImageData.generateSkewedPage("_page/final/", "_page/skewed");

        //Membuat kounter citra dokumen miring
        GenerateImageData.generateFixedSkewedPage_moment("_page/skewed/", "_test_result/skew_moment2/");
        GenerateImageData.generateFixedSkewedPage_triangle("_page/skewed/", "_test_result/skew_triangle/");
    }
}
