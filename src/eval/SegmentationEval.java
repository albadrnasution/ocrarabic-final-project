/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eval;

import image.BinaryImageShell;
import image.MainbodySOSet;
import image.classifier.Classifier;
import image.classifier.FeatureExtraction;
import image.segmentator.SegmentatorChar;
import image.segmentator.SegmentatorLine;
import image.segmentator.SegmentatorSubword;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author albadr.ln
 */
public class SegmentationEval {

    String path = "_test_result/segmentasi - " + new SimpleDateFormat("yyyyMMdd_HHmm").format(Calendar.getInstance().getTime()) + "/";
    //which one to save?
    boolean saveLetterSegment = true;
    boolean saveWordSegment = true;
    boolean saveLineSegment = true;
    boolean ignoreWordOperation = false;
    //prefixes, give trailing slash "/" to make it a folder
    String linePrefix = "line";
    String wordPrefix = "subwords/";
    String letterPrefix = "";

    public SegmentationEval() {
        File[] files = new File[1];
        files[0] = new File("_page/1 - arial.png");
        this.saveDocumentsSegments(files);
    }

    public SegmentationEval(File file) {
        if (file.isDirectory()) {
            // ada beberapa folder yang dipilih
            File[] files = file.listFiles();
            // save segments
            this.saveDocumentsSegments(files);

        } else if (file.isFile()) {
            // hanya satu file yang dipilih
            // save segments
            File[] files = new File[1];
            files[0] = file;
            this.saveDocumentsSegments(files);
        }
    }

    /**
     * Melakukan segmentasi baris, upakata, dan huruf dari kumpulan citra
     * dokumen. Citra hasil segmentasi setiap citra dokumen akan disimpan pada
     * folder berisi nama berkas citra dokumen tersebut.
     *
     * @param docFiles Daftar seluruh citra dokumen yang ingin disegmen
     */
    private void saveDocumentsSegments(File[] docFiles) {
        Classifier cl = new Classifier();
        cl.loadDataDefault();

        // set array
        for (int i = 0; i < docFiles.length; ++i) {
            if (docFiles[i].isDirectory()) {
                continue;
            }

            int docIndex = i;
            BinaryImageShell docImages = new BinaryImageShell(docFiles[i]);
            docImages.cropEnvelope();
            docImages.updateImage();
            docImages.repaint();

            /* Initialize segmentator line for this documentImage*/
            SegmentatorLine segmentatorLine = new SegmentatorLine(docImages);
            segmentatorLine.findSeparationLine();
            segmentatorLine.separate();

            BinaryImageShell[] linesSegment = segmentatorLine.getSegments();

            /* Segment every line-segments*/
            for (int lineIndex = 0; lineIndex < linesSegment.length; ++lineIndex) {
                BinaryImageShell lineImage = linesSegment[lineIndex];
                lineImage.cropEnvelope();
                lineImage.updateImage();
                lineImage.resizeTo64();
                lineImage.binarize();
                lineImage.repaint();

                SegmentatorSubword segmentatorWord = new SegmentatorSubword(lineImage);
                segmentatorWord.blockSegment();
                segmentatorWord.groupBlocks();

                /*Save lineimage*/
                String docName = docFiles[docIndex].getName();
                String linepath = this.path + docName + "/" + String.format("%02d", lineIndex);
                if (saveLineSegment) {
                    String linepathImage = this.path + docName + "/" + linePrefix + String.format("%02d", lineIndex);
                    lineImage.saveImage(linepathImage);
                }

                /* Segment every word-segments*/
                if (!ignoreWordOperation) {
                    for (int wordIndex = 0; wordIndex < segmentatorWord.sizeSegments(); ++wordIndex) {
                        MainbodySOSet wordSegment = segmentatorWord.getSegment(wordIndex);
                        SegmentatorChar segmentatorChar = new SegmentatorChar(wordSegment);
                        segmentatorChar.zidouri();

                        /*Save wordimage*/
                        String wordpath = linepath + "/" + String.format("%02d", wordIndex);
                        if (saveWordSegment) {
                            String wordimagepath = linepath + "/" + wordPrefix + String.format("%02d", wordIndex);
                            BinaryImageShell wordimage = segmentatorChar.getInputImage_withSecondary();
                            wordimage.saveImage(wordimagepath);
                        }

                        /*Save every char-segments*/
                        if (saveLetterSegment) {
                            for (int letterIndex = 0; letterIndex < segmentatorChar.getSegmentSize(); ++letterIndex) {
                                BinaryImageShell letterImage = segmentatorChar.getChar_withSecondary(letterIndex);
                                Instances dataUnlabeled = new Instances("Rel", cl.getFeatureVector(), 0);
                                //Guessing
                                Instance in = cl.extractFeature(segmentatorChar.getChar_plain(letterIndex));
                                dataUnlabeled.add(in);
                                String code = cl.classify(dataUnlabeled.firstInstance());
                                int[][] secCount = FeatureExtraction.defineSecObject(segmentatorChar.getSecondaries(letterIndex), segmentatorWord.getBaseline());
                                //To the letter
                                String letter = Classifier.letter(code, secCount);
                                //Saving
                                String letterPath = wordpath + letterPrefix + String.format("%02d", letterIndex) + " - " + letter;
                                letterImage.saveImage(letterPath);
                            }
                        }
                    }
                }
            }
        }

        System.out.println("Save Complete.");
    }

    public static void main(String args[]) {
        String path = "_page/final/";

        System.out.println("Starting test...");

        long before = System.currentTimeMillis();
        SegmentationEval uji = new SegmentationEval(new File(path));
        long after = System.currentTimeMillis();

        System.out.println("Test takes " + (after - before) + " ms");

    }
}
