/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eval;

import image.BinaryImageShell;
import image.classifier.FeatureExtraction;
import image.segmentator.Hilditch;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @deprecated 
 * @author albadr.ln
 */
public class UsingWeka {
    // Declare attributes
    Attribute classAttribute;
    Attribute boundary1 = new Attribute("bdPerimeterLength");
    Attribute boundary2 = new Attribute("bdPerimeterToDiagonal");
    Attribute boundary3 = new Attribute("bdCompactnessRatio");
    Attribute boundary4 = new Attribute("bdBendingEnergy");
    Attribute density1 = new Attribute("dTopToBottom");
    Attribute density2 = new Attribute("dLeftToRight");
    Attribute density3 = new Attribute("dTopLeftToAcre");
    Attribute density4 = new Attribute("dTopRightToAcre");
    Attribute density5 = new Attribute("dBottomLeftToAcre");
    Attribute density6 = new Attribute("dBottomRightToAcre");
    Attribute mainBody1_aspectRatio = new Attribute("mbAspectRatio");
    Attribute mainBody2_loop = new Attribute("mbLoopCount");
    ArrayList<Attribute> interestPointAtt = new ArrayList<>();

    public UsingWeka(File trainingSet, File testingSet) {

        if (trainingSet.isDirectory()) {
            // ada beberapa folder yang dipilih
            File[] trainingFiles = trainingSet.listFiles();
            File[] testingFiles = testingSet.listFiles();
            // save segments
            this.ujiUsingWeka(trainingFiles, testingFiles);
            System.out.println("===========");
        } else if (trainingSet.isFile()) {
            // hanya satu file yang dipilih
            // save segments
            File[] trainingFiles = new File[1];
            File[] testingFiles = new File[1];
            trainingFiles[0] = trainingSet;
            testingFiles[0] = testingSet;
            this.ujiUsingWeka(trainingFiles, testingFiles);
        }

    }

    public UsingWeka(File[] trainingSet, File[] testingSet) {
        // ada beberapa folder yang dipilih
        ArrayList<File> trainingFiles = new ArrayList<>();
        ArrayList<File> testingFiles = new ArrayList<>();

        for (File tf : trainingSet) {
            File[] files = tf.listFiles();
            trainingFiles.addAll(Arrays.asList(files));
        }
        for (File tf : testingSet) {
            File[] files = tf.listFiles();
            testingFiles.addAll(Arrays.asList(files));
        }

        // save segments
        this.ujiUsingWeka(trainingFiles.toArray(trainingSet), testingFiles.toArray(testingSet));
    }

    /**
     * Menginisiasi tempat penyimpan fitur-fitur huruf.
     *
     * @return
     */
    public FastVector declareFeatureVector() {
        interestPointAtt.add(new Attribute("ipEnd_q0"));
        interestPointAtt.add(new Attribute("ipEnd_q1"));
        interestPointAtt.add(new Attribute("ipEnd_q2"));
        interestPointAtt.add(new Attribute("ipEnd_q3"));
        interestPointAtt.add(new Attribute("ipBranch_q0"));
        interestPointAtt.add(new Attribute("ipBranch_q1"));
        interestPointAtt.add(new Attribute("ipBranch_q2"));
        interestPointAtt.add(new Attribute("ipBranch_q3"));
        interestPointAtt.add(new Attribute("ipCross_q0"));
        interestPointAtt.add(new Attribute("ipCross_q1"));
        interestPointAtt.add(new Attribute("ipCross_q2"));
        interestPointAtt.add(new Attribute("ipCross_q3"));

        // Declare the class attribute along with its values
        // @todo set semua kode badan utama huruf disini
        FastVector fvClassVal = new FastVector();
        fvClassVal.addElement("AA0");
        fvClassVal.addElement("AA1");
        fvClassVal.addElement("AA2");
        fvClassVal.addElement("AA3");
        fvClassVal.addElement("AA4");
        fvClassVal.addElement("AA5");
        fvClassVal.addElement("AA6");
        fvClassVal.addElement("AA7");
        fvClassVal.addElement("AA8");
        fvClassVal.addElement("AA9");
        fvClassVal.addElement("S10");
        fvClassVal.addElement("000");
        fvClassVal.addElement("001");
        fvClassVal.addElement("010");
//       fvClassVal.addElement("011");
        fvClassVal.addElement("012");
        fvClassVal.addElement("013");
        fvClassVal.addElement("020");
//        fvClassVal.addElement("021");
        fvClassVal.addElement("022");
//        fvClassVal.addElement("023");
        fvClassVal.addElement("030");
//        fvClassVal.addElement("031");
        fvClassVal.addElement("040");
//        fvClassVal.addElement("041");
        fvClassVal.addElement("050");
//        fvClassVal.addElement("051");
        fvClassVal.addElement("052");
//        fvClassVal.addElement("053");
        fvClassVal.addElement("060");
//        fvClassVal.addElement("061");
        fvClassVal.addElement("062");
//        fvClassVal.addElement("063");
        fvClassVal.addElement("070");
//        fvClassVal.addElement("071");
//        fvClassVal.addElement("072");
//        fvClassVal.addElement("073");
        fvClassVal.addElement("080");
        fvClassVal.addElement("081");
        fvClassVal.addElement("082");
        fvClassVal.addElement("083");
        fvClassVal.addElement("090");
//        fvClassVal.addElement("091");
//        fvClassVal.addElement("092");
//        fvClassVal.addElement("093");
        fvClassVal.addElement("100");
//        fvClassVal.addElement("101");
        fvClassVal.addElement("102");
//        fvClassVal.addElement("103");
        fvClassVal.addElement("110");
//        fvClassVal.addElement("111");
        fvClassVal.addElement("112");
//        fvClassVal.addElement("113");
        fvClassVal.addElement("120");
//        fvClassVal.addElement("121");
        fvClassVal.addElement("130");
        fvClassVal.addElement("131");
        fvClassVal.addElement("132");
        fvClassVal.addElement("133");
        fvClassVal.addElement("140");
        fvClassVal.addElement("141");
//        fvClassVal.addElement("142");
//        fvClassVal.addElement("143");
        classAttribute = new Attribute("mbClassCode", fvClassVal);

        // Declare the feature vector
        FastVector fvWekaAttributes = new FastVector();
        fvWekaAttributes.addElement(classAttribute);
        fvWekaAttributes.addElement(boundary1);
        fvWekaAttributes.addElement(boundary2);
        fvWekaAttributes.addElement(boundary3);
        fvWekaAttributes.addElement(boundary4);
        fvWekaAttributes.addElement(density1);
        fvWekaAttributes.addElement(density2);
        fvWekaAttributes.addElement(density3);
        fvWekaAttributes.addElement(density4);
        fvWekaAttributes.addElement(density5);
        fvWekaAttributes.addElement(density6);
        fvWekaAttributes.addElement(mainBody1_aspectRatio);
//        fvWekaAttributes.addElement(mainBody2);
        for (int iip = 0; iip < 12; ++iip) {
            fvWekaAttributes.addElement(interestPointAtt.get(iip));
        }

        return fvWekaAttributes;
    }

    /**
     *
     * @param fvWekaAttributes
     * @param image Citra yang sudah diamplopin ya
     * @return
     */
    public Instance extractFeature(FastVector fvWekaAttributes, BinaryImageShell image, String letterCode) {
        //prepare instance
        Instance instance = new Instance(24);
        //get feature
        ArrayList<Integer> boundaryChainCode = FeatureExtraction.chainCodeBoundary(image);
        double[] distribution = FeatureExtraction.distribution(image);
        double perimeter = FeatureExtraction.perimeterLength(boundaryChainCode);
        double perimeterToDiag = FeatureExtraction.perimeterToDiagonalRatio(perimeter, image.getWidth(), image.getHeight());
        double compactness = FeatureExtraction.compactnessRatio(perimeter, (int) distribution[FeatureExtraction.ID_ACRE]);
        double bendingEnergy = FeatureExtraction.bendingEnergy(perimeter, boundaryChainCode);
        double aspectRatio = FeatureExtraction.aspectRatio(image);
        //int loop = FeatureExtraction.countLoop(image);

        //Skeleton?
        Hilditch hilditch = new Hilditch(image);
        BinaryImageShell skeleton = hilditch.getSkeleton();
        int[][] ip = FeatureExtraction.interestPoint(skeleton);

        //set instance
        instance.setValue(classAttribute, letterCode);
        //set instance : boundary   
        instance.setValue(boundary1, perimeter);
        instance.setValue(boundary2, perimeterToDiag);
        instance.setValue(boundary3, compactness);
        instance.setValue(boundary4, bendingEnergy);
        //set instance : distribution
        instance.setValue(density1, distribution[FeatureExtraction.ID_TOP_PER_BOTTOM]);
        instance.setValue(density2, distribution[FeatureExtraction.ID_LEFT_PER_RIGHT]);
        instance.setValue(density3, distribution[FeatureExtraction.ID_TOPLEFT_PER_ACRE]);
        instance.setValue(density4, distribution[FeatureExtraction.ID_TOPRIGHT_PER_ACRE]);
        instance.setValue(density5, distribution[FeatureExtraction.ID_BOTTOMLEFT_PER_ACRE]);
        instance.setValue(density6, distribution[FeatureExtraction.ID_BOTTOMRIGHT_PER_ACRE]);
        //set instance : main body
        // @todo count loop?
        instance.setValue(mainBody1_aspectRatio, aspectRatio);
        //instance.setValue(mainBody2_loop, loop);
        //set intance: interest point
        for (int iip = 0; iip < 12; ++iip) {
            instance.setValue(interestPointAtt.get(iip), ip[iip / 4][iip % 4]);
        }

        return instance;
    }

    private void ujiUsingWeka(File[] trainingFile, File[] testingFile) {
        try {
            // Declare feature vector
            FastVector fvWekaAttributes = declareFeatureVector();

            // Create an empty training set
            Instances trainingSet = this.setInstances(fvWekaAttributes, trainingFile);
            // Print ARFF
            //System.out.println(trainingSet);

            // J48 classifier
            Classifier cModel = (Classifier) new J48();
            //cModel.buildClassifier(trainingSet);

            // Add testing data
            Instances testingSet = this.setInstances(fvWekaAttributes, testingFile);

            // Test the model
            Evaluation eTest = new Evaluation(trainingSet);
            //eTest.evaluateModel(cModel, testingSet);
            eTest.crossValidateModel(cModel, trainingSet, 10, new Random(1));

            // Print the result à la Weka explorer:
            String strSummary = eTest.toSummaryString();
            System.out.println(strSummary);
            //System.out.println(eTest.toClassDetailsString());

            // Get the confusion matrix
            //double[][] cmMatrix = eTest.confusionMatrix();


        } catch (Exception ex) {
            Logger.getLogger(UsingWeka.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Instances setInstances(FastVector fvWekaAttributes, File files[]) {
        // Create an empty training set
        Instances trainingSet = new Instances("Rel", fvWekaAttributes, 10);
        // Set class index
        trainingSet.setClassIndex(0);

        // Add instances for training data
        for (File file : files) {
            BinaryImageShell image = new BinaryImageShell(file);
            image.cropEnvelope();
            image.updateImage();
            String letterCode = file.getName().substring(0, 3);

            Instance ins = this.extractFeature(fvWekaAttributes, image, letterCode);
            trainingSet.add(ins);
        }

        return trainingSet;
    }

    public static void main(String args[]) {
        File[] trainingSet = new File[12];
        trainingSet[0] = new File("_raw/mainbody/01/");
        trainingSet[1] = new File("_raw/mainbody/02/");
        trainingSet[2] = new File("_raw/mainbody/03/");
        trainingSet[3] = new File("_raw/mainbody/04/");
        trainingSet[4] = new File("_raw/mainbody/05/");
        trainingSet[5] = new File("_raw/mainbody/06/");
        trainingSet[6] = new File("_raw/mainbody/bold01/");
        trainingSet[7] = new File("_raw/mainbody/bold02/");
        trainingSet[8] = new File("_raw/mainbody/bold03/");
        trainingSet[9] = new File("_raw/mainbody/bold04/");
        trainingSet[10] = new File("_raw/mainbody/bold05/");
        trainingSet[11] = new File("_raw/mainbody/bold06/");

        File[] testingSet = new File[6];
        testingSet[0] = new File("_raw/mainbody/01/");
        testingSet[1] = new File("_raw/mainbody/02/");
        testingSet[2] = new File("_raw/mainbody/03/");
        testingSet[3] = new File("_raw/mainbody/04/");
        testingSet[4] = new File("_raw/mainbody/05/");
        testingSet[5] = new File("_raw/mainbody/06/");
        
        
        File[] trainingSetFont = new File[10];
        int j = 0;
        for (int i=1; i <= 6; ++i){
            //yang mau di skip (font yg jadi data uji) tulis di sini
            if (i==6) continue;
            //masukin data latih
            trainingSetFont[j*2] = new File("_raw/mainbody/0"+i+"/");
            trainingSetFont[j*2 + 1] = new File("_raw/mainbody/bold0"+i+"/");
            j++;
        }
        
        File[] testingSetFont = new File[2];
        testingSetFont[0] = new File("_raw/mainbody/06/");
        testingSetFont[1] = new File("_raw/mainbody/bold06/");

        System.out.println("Starting uji weka...");

        long before = System.currentTimeMillis();
        UsingWeka uji = new UsingWeka(trainingSet, testingSet);
        long after = System.currentTimeMillis();

        System.out.println("Test uji weka " + (after - before) + " ms");
    }
}
