/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package image.classifier;

import image.BinaryImageShell;
import image.segmentator.Hilditch;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author albadr.ln
 */
public class Classifier {

    //Database path
    private String defaultDataPath = "data.arff";
    //The Database
    Instances database;
    //The real classifier
    weka.classifiers.Classifier cModel;
    //Feature vector
    FastVector fvWekaAttributes;
    // Declare attributes
    Attribute classAttribute;
    HashMap<String, Attribute> boundaryAtt = new HashMap<>();
    ArrayList<Attribute> density = new ArrayList<>();
    ArrayList<Attribute> interestPointAtt = new ArrayList<>();
    Attribute mainBody1_aspectRatio = new Attribute("mbAspectRatio");
    Attribute mainBody2_loop = new Attribute("mbLoopCount");

    public Classifier() {
        init();
    }

    private void init() {
        try {
            // Declare feature vector
            this.fvWekaAttributes = declareFeatureVector();
            // Declare classifier
            this.cModel = (weka.classifiers.Classifier) new J48();
        } catch (Exception ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Mengelompokkan data baru yang belum diketahui berdasarkan pohon keputusan
     * yang ada.
     *
     * @param unlabeled Data tak diketahui.
     * @return Kelas tebakan dari data.
     */
    public String classify(Instance unlabeled) {
        try {
            unlabeled.setDataset(database);
            double clsLabel = this.cModel.classifyInstance(unlabeled);
            //System.out.println(clsLabel + " -> " + unlabeled.classAttribute().value((int) clsLabel));

            return unlabeled.classAttribute().value((int) clsLabel);
        } catch (Exception ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Membuka data pada berkas ARFF standar yang diset pada atribut kelas.
     */
    public void loadDataDefault() {
        try {
            // Load saved instances
            this.database = openARFF(defaultDataPath);
            this.cModel.buildClassifier(database);
        } catch (Exception ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Membuka data berkas ARFF pada path tertentu. Data ARFF kemudian menjadi
     * database dari classifier.
     *
     * @param arffPath Lokasi berkas ARFF yang ingin dibuka.
     */
    public void loadData(String arffPath) {
        try {
            // Load saved instances
            this.database = openARFF(arffPath);
            this.cModel.buildClassifier(database);
        } catch (Exception ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Memuat daftar instans data latih dari daftar instans yang ada ke database
     * kelas classifier.
     *
     * @param data
     */
    public void loadData(Instances data) {
        try {
            // Load saved instances
            this.database = data;
            this.cModel.buildClassifier(database);
        } catch (Exception ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Memuat data pada berkas ARFF.
     *
     * @param path
     * @return
     */
    public static Instances openARFF(String path) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(path));
            Instances data;
            data = new Instances(reader);
            // setting class attribute
            data.setClassIndex(0);
            //System.out.println(data);

            return data;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Menyimpan daftar instans pada lokasi tertentu sebagai berkas ARFF.
     *
     * @param data Daftar instans yang ingin disimpan.
     * @param path Lokasi berkas. Tambahi .arff pada parameter ini.
     */
    public void saveARFF(Instances data, String path) {
        try (PrintWriter out = new PrintWriter(path)) {
            out.print(data);
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Classifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Membuat sebuah instan dengan label tertentu dari sebuah citra huruf.
     * Fitur huruf tersebut diekstrak sesuai feature vector.
     *
     * @param fvWekaAttributes
     * @param image Citra huruf yang sudah dilakukan envelope
     * @return
     */
    public Instance extractFeature(BinaryImageShell image, String letterCode) {
        //prepare instance
        Instance instance = extractFeature(image);

        //set instance : class
        instance.setValue(classAttribute, letterCode);

        return instance;
    }

    /**
     * Membuat instan dengan kelas tidak diketahui dari sebuah citra huruf.
     * Fitur huruf tersebut diekstrak sesuai feature vector.
     *
     * @param fvWekaAttributes
     * @param image Citra huruf yang sudah dilakukan envelope
     * @return
     */
    public Instance extractFeature(BinaryImageShell image) {
        Instance instance = new Instance(25);

        //Boundary Feature
        ArrayList<Integer> boundaryChainCode = FeatureExtraction.chainCodeBoundary(image);
        double[] distribution = FeatureExtraction.distribution(image);
        double perimeter = FeatureExtraction.perimeterLength(boundaryChainCode);
        double perimeterToDiag = FeatureExtraction.perimeterToDiagonalRatio(perimeter, image.getWidth(), image.getHeight());
        double compactness = FeatureExtraction.compactnessRatio(perimeter, (int) distribution[FeatureExtraction.ID_ACRE]);
        double bendingEnergy = FeatureExtraction.bendingEnergy(perimeter, boundaryChainCode);

        //Main Body Feature
        double aspectRatio = FeatureExtraction.aspectRatio(image);
        int loop = FeatureExtraction.countLoop_byCC(image);

        //Skeleton Feature
        Hilditch hilditch = new Hilditch(image);
        BinaryImageShell skeleton = hilditch.getSkeleton();
        int[][] ip = FeatureExtraction.interestPoint(skeleton);

        //set instance : boundary   
        instance.setValue(boundaryAtt.get("bdPerimeterLength"), perimeter);
        instance.setValue(boundaryAtt.get("bdPerimeterToDiagonal"), perimeterToDiag);
        instance.setValue(boundaryAtt.get("bdCompactnessRatio"), compactness);
        instance.setValue(boundaryAtt.get("bdBendingEnergy"), bendingEnergy);
        //set instance : distribution
        instance.setValue(density.get(0), distribution[FeatureExtraction.ID_TOP_PER_BOTTOM]);
        instance.setValue(density.get(1), distribution[FeatureExtraction.ID_LEFT_PER_RIGHT]);
        instance.setValue(density.get(2), distribution[FeatureExtraction.ID_TOPLEFT_PER_ACRE]);
        instance.setValue(density.get(3), distribution[FeatureExtraction.ID_TOPRIGHT_PER_ACRE]);
        instance.setValue(density.get(4), distribution[FeatureExtraction.ID_BOTTOMLEFT_PER_ACRE]);
        instance.setValue(density.get(5), distribution[FeatureExtraction.ID_BOTTOMRIGHT_PER_ACRE]);
        //set instance : main body
        instance.setValue(mainBody1_aspectRatio, aspectRatio);
        instance.setValue(mainBody2_loop, loop);
        //set intance: interest point
        for (int iip = 0; iip < 12; ++iip) {
            instance.setValue(interestPointAtt.get(iip), ip[iip / 4][iip % 4]);
        }

        return instance;
    }

    /**
     * Mengembalikan vektor fitur yang ada di kelas classifier ini.
     *
     * @return Vektor fitur.
     */
    public FastVector getFeatureVector() {
        return this.fvWekaAttributes;
    }

    /**
     * Menginisiasi tempat penyimpan fitur-fitur huruf.
     *
     * @return Vector fitur huruf yang diinisiasi.
     */
    private FastVector declareFeatureVector() {
        //Init attributes of boundary
        boundaryAtt.put("bdPerimeterLength", new Attribute("bdPerimeterLength"));
        boundaryAtt.put("bdPerimeterToDiagonal", new Attribute("bdPerimeterToDiagonal"));
        boundaryAtt.put("bdCompactnessRatio", new Attribute("bdCompactnessRatio"));
        boundaryAtt.put("bdBendingEnergy", new Attribute("bdBendingEnergy"));

        //Init attributes of density
        density.add(new Attribute("dTopToBottom"));
        density.add(new Attribute("dLeftToRight"));
        density.add(new Attribute("dTopLeftToAcre"));
        density.add(new Attribute("dTopRightToAcre"));
        density.add(new Attribute("dBottomLeftToAcre"));
        density.add(new Attribute("dBottomRightToAcre"));

        //Init attributes of interest point
        interestPointAtt.add(new Attribute("ipEnd_q0"));
        interestPointAtt.add(new Attribute("ipEnd_q1"));
        interestPointAtt.add(new Attribute("ipEnd_q2"));
        interestPointAtt.add(new Attribute("ipEnd_q3"));
        interestPointAtt.add(new Attribute("ipBranch_q0"));
        interestPointAtt.add(new Attribute("ipBranch_q1"));
        interestPointAtt.add(new Attribute("ipBranch_q2"));
        interestPointAtt.add(new Attribute("ipBranch_q3"));
        interestPointAtt.add(new Attribute("ipCross_q0"));
        interestPointAtt.add(new Attribute("ipCross_q1"));
        interestPointAtt.add(new Attribute("ipCross_q2"));
        interestPointAtt.add(new Attribute("ipCross_q3"));

        // Declare the class attribute along with its values
        FastVector fvClassVal = new FastVector();
        fvClassVal.addElement("AA0");
        fvClassVal.addElement("AA1");
        fvClassVal.addElement("AA2");
        fvClassVal.addElement("AA3");
        fvClassVal.addElement("AA4");
        fvClassVal.addElement("AA5");
        fvClassVal.addElement("AA6");
        fvClassVal.addElement("AA7");
        fvClassVal.addElement("AA8");
        fvClassVal.addElement("AA9");
        fvClassVal.addElement("S00");
        fvClassVal.addElement("000");
        fvClassVal.addElement("001");
        fvClassVal.addElement("010");
//       fvClassVal.addElement("011");
        fvClassVal.addElement("012");
        fvClassVal.addElement("013");
        fvClassVal.addElement("020");
//        fvClassVal.addElement("021");
        fvClassVal.addElement("022");
//        fvClassVal.addElement("023");
        fvClassVal.addElement("030");
//        fvClassVal.addElement("031");
        fvClassVal.addElement("040");
//        fvClassVal.addElement("041");
        fvClassVal.addElement("050");
//        fvClassVal.addElement("051");
        fvClassVal.addElement("052");
//        fvClassVal.addElement("053");
        fvClassVal.addElement("060");
//        fvClassVal.addElement("061");
        fvClassVal.addElement("062");
//        fvClassVal.addElement("063");
        fvClassVal.addElement("070");
//        fvClassVal.addElement("071");
//        fvClassVal.addElement("072");
//        fvClassVal.addElement("073");
        fvClassVal.addElement("080");
        fvClassVal.addElement("081");
        fvClassVal.addElement("082");
        fvClassVal.addElement("083");
        fvClassVal.addElement("090");
//        fvClassVal.addElement("091");
//        fvClassVal.addElement("092");
//        fvClassVal.addElement("093");
        fvClassVal.addElement("100");
//        fvClassVal.addElement("101");
        fvClassVal.addElement("102");
//        fvClassVal.addElement("103");
        fvClassVal.addElement("110");
//        fvClassVal.addElement("111");
        fvClassVal.addElement("112");
//        fvClassVal.addElement("113");
        fvClassVal.addElement("120");
//        fvClassVal.addElement("121");
        fvClassVal.addElement("130");
        fvClassVal.addElement("131");
        fvClassVal.addElement("132");
        fvClassVal.addElement("133");
        fvClassVal.addElement("140");
        fvClassVal.addElement("141");
//        fvClassVal.addElement("142");
//        fvClassVal.addElement("143");
        fvClassVal.addElement("152");
        classAttribute = new Attribute("mbClassCode", fvClassVal);

        // Declare the feature vector
        FastVector fv = new FastVector();
        ///class
        fv.addElement(classAttribute);
        ///boundary att
        fv.addElement(boundaryAtt.get("bdPerimeterLength"));
        fv.addElement(boundaryAtt.get("bdPerimeterToDiagonal"));
        fv.addElement(boundaryAtt.get("bdCompactnessRatio"));
        fv.addElement(boundaryAtt.get("bdBendingEnergy"));
        ///density att
        fv.addElement(density.get(0));
        fv.addElement(density.get(1));
        fv.addElement(density.get(2));
        fv.addElement(density.get(3));
        fv.addElement(density.get(4));
        fv.addElement(density.get(5));
        ///mainbody att
        fv.addElement(mainBody1_aspectRatio);
        fv.addElement(mainBody2_loop);
        ///interest point att
        for (int iip = 0; iip < 12; ++iip) {
            fv.addElement(interestPointAtt.get(iip));
        }

        return fv;
    }

    /**
     * Memembuat kelompok instan dari kumpulan file citra huruf.
     *
     * @param fvWekaAttributes
     * @param files
     * @return
     */
    public Instances setInstances(File files[]) {
        // Create an empty training set
        Instances trainingSet = new Instances("Rel", fvWekaAttributes, 10);
        // Set class index
        trainingSet.setClassIndex(0);

        // Add instances for training data
        for (File file : files) {
            BinaryImageShell image = new BinaryImageShell(file);
            image.cropEnvelope();
            //penyamaan ukuran huruf 64x64 disini? supaya panjang keliling serasio
            image.updateImage();
            String letterCode = file.getName().substring(0, 3);

            Instance ins = this.extractFeature(image, letterCode);
            trainingSet.add(ins);
        }

        return trainingSet;
    }

    public static String letter(String mainbodyCode, int[][] secondaries) {

        int nTitikAtas = secondaries[FeatureExtraction.ID_SO_ATAS][FeatureExtraction.ID_SO_TITIK];
        int nTitikBawah = secondaries[FeatureExtraction.ID_SO_BAWAH][FeatureExtraction.ID_SO_TITIK];
        int nHamzahAtas = secondaries[FeatureExtraction.ID_SO_ATAS][FeatureExtraction.ID_SO_ZIGZAG_HAMZAH];
        int nHamzahBawah = secondaries[FeatureExtraction.ID_SO_BAWAH][FeatureExtraction.ID_SO_ZIGZAG_HAMZAH];

        switch (mainbodyCode) {
            case "000":
            case "001":
                //alif
                if (nHamzahAtas == 1) {
                    return "\u0623‎";
                } else if (nHamzahBawah == 1) {
                    return "\u0625";
                } else {
                    return "\u0627";
                }
            case "010":
                //awal-tunggal: ba', ta', tsa', nun
                if (nTitikBawah == 1 && nTitikAtas == 0) {
                    //ba'
                    return "\u0628";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //nun
                    return "\u0646";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //ta
                    return "\u062a";
                } else if (nTitikBawah == 0 && nTitikAtas == 3) {
                    //tsa
                    return "\u062b";
                } else if (nTitikBawah == 2 && nTitikAtas == 0) {
                    //ya
                    return "\u064a";
                } else if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //'ain, dari experimen mirip soalnya
                    return "\u0639";
                }
                break;
            case "012":
            case "013":
                //tengah-akhir: ba', ta', tsa', nun, ya
                if (nTitikBawah == 1 && nTitikAtas == 0) {
                    //ba'
                    return "\u0628";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //nun
                    return "\u0646";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //ta
                    return "\u062a";
                } else if (nTitikBawah == 0 && nTitikAtas == 3) {
                    //tsa
                    return "\u062b";
                } else if (nTitikBawah == 2 && nTitikAtas == 0) {
                    //ya
                    return "\u064a";
                } else if (nHamzahAtas == 1 && nTitikAtas == 0 && nTitikBawah == 0) {
                    //ya tanpa titik dua dengan hamzah
                    return "\u0626";
                } else {
                    //ya tanpa titik dua dan tanpa hamzah
                    return "\u0649";
                }
            case "020":
            //awal-tunggal: jim, hah, khah.
            case "022":
                //tengah-akhir: jim, hah, khah.
                if (nTitikBawah == 1 && nTitikAtas == 0) {
                    //jim
                    return "\u062c";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //hah
                    return "\u062d";
                } else if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //khah
                    return "\u062e";
                }
                break;
            case "030":
                //akhir-tunggal: dal, dzal.
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //dal
                    return "\u062f";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //dzal
                    return "\u0630";
                } else if (nTitikBawah == 0 && nTitikAtas == 3) {
                    //ternyata ada titik tiga, wow!!! Kembalikan sebagai syin aja.
                    //yang ada titik tiga cuma tsa
                    return "\u062b";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //ternyata ada titik dua, wow!!! Kembalikan sebagai ta aja.
                    //badannya mirip terus yg ada titik dua cuma ta
                    return "\u062a";
                } else if (nTitikBawah == 1 && nTitikAtas == 0) {
                    //ternyata ada titik satu dibawah, wow!!! Kembalikan sebagai ba aja.
                    //yg badannya mirip terus ada titik dibawah
                    return "\u0628";
                } else if (nTitikBawah == 2 && nTitikAtas == 0) {
                    //ternyata ada titik dua dibawah, wow!!! Kembalikan sebagai ya aja.
                    //yg badannya mirip terus ada dua titik dibawah
                    return "\u064a";
                }
                break;
            case "040":
                //akhir-tunggal: ra, zain,
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //ra
                    return "\u0631";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //zain, ada titiknya di atas
                    return "\u0632";
                }
                break;
            case "050":
            //awal-tunggal: siin, shiin
            case "052":
                //tengah-akhir: siin, shiin
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //siin
                    return "\u0633";
                } else if (nTitikBawah == 0 && nTitikAtas != 0) {
                    //shiin, ada titiknya di atas
                    return "\u0634";
                }
                break;
            case "060":
            //awal-tunggal: sad, dad
            case "062":
                //tengah-akhir: sad, dad
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //sad
                    return "\u0635";
                } else if (nTitikBawah == 0 && nTitikAtas != 0) {
                    //dad, ada titiknya di atas
                    return "\u0636";
                }
                break;
            case "070":
                //tunggal-awal-tengah-akhir: thah, dzah
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //thah
                    return "\u0637";
                } else if (nTitikBawah == 0 && nTitikAtas != 0) {
                    //dzah, ada titiknya di atas
                    return "\u0638";
                }
                break;
            case "080":
            //tunggl: 'ain, gain
            case "081":
            //akhir : 'ain, gain
            case "082":
            //tengah: 'ain, gain
            case "083":
                //awal  : 'ain, gain
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //'ain
                    return "\u0639";
                } else if (nTitikBawah == 0 && nTitikAtas != 0) {
                    //gain, ada titiknya di atas
                    return "\u0631";
                }
                break;
            case "090":
                //tunggal-awal-tengah-akhir: fa, qaf
                if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //fa, titiknya satu
                    return "\u0641";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //qaf, titiknya dua
                    return "\u0642";
                } else {
                    //kalau bukan, waw aja deh
                    return "\u0648";
                }
            case "100":
                //awal-tunggal: lam
                if (nHamzahAtas == 0) {
                    //lam, obviously
                    return "\u0644";
                } else if (nHamzahAtas > 0) {
                    //kalau ada hamzah, kaf
                    return "\u0643";
                }
                break;
            case "102":
                //tengah-akhir: lam
                //TODO cek fitur benda disini, kalau pendek berarti bukan lam?
                if (nTitikBawah == 1 && nTitikAtas == 0) {
                    //lain-lain yg mirip lam: ba'
                    return "\u0628";
                } else if (nTitikBawah == 2 && nTitikAtas == 0) {
                    //lain-lain yg mirip lam: ya'
                    return "\u064a";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //nun
                    return "\u0646";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //ta
                    return "\u062a";
                } else if (nTitikBawah == 0 && nTitikAtas == 3) {
                    //tsa
                    return "\u062b";
                }
                return "\u0644";
            case "110":
                //awal-tunggal: mim
                return "\u0645";
            case "112":
                //tengah-akhir: mim
                //TODO cek fitur benda yang mirip mim, sin, syin?
                return "\u0645";
            case "120":
                //tunggal-akhir: waw
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //waw
                    return "\u0648";
                } else if (nHamzahAtas == 1){
                    //waw dengan hamzah
                    return "\u0624";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //yang mirip, dan bertitik : zain
                    return "\u0631";
                }
                //default: waw
                return "\u0648";
            case "130":
            case "131":
                //tunggal-akhir: hah
                if (nTitikBawah == 0 && nTitikAtas == 0) {
                    //hah biasa
                    return "\u0647";
                } else if (nTitikBawah == 0 && nTitikAtas != 0) {
                    //ta marbutah, setidaknya ada titik satu
                    return "\u0629";
                }
                break;
            case "132":
            case "133":
                //semua: hah
                return "\u0647";
            case "140":
            case "141":
                //tunggal-akhir: ya
                if (nTitikBawah == 0 && nHamzahAtas == 0) {
                    //ya tanpa titik tanpa hamzah
                    return "\u0649";
                } else if (nTitikBawah == 2 && nHamzahAtas == 0) {
                    //ya bertitik tanpa hamzah
                    return "\u064a";
                } else if (nHamzahAtas == 1) {
                    //ya tanpa titik berhamzah
                    return "\u0626";
                }
                break;
            case "152":
                //awal-tengah: kaf
                return "\u0643";
            case "S00":
                //tunggal-akhir: lamalif
                if (nHamzahAtas == 1) {
                    //lamalif dengan alif berhamzah atas
                    return "\u0644\u0623";
                } else if (nHamzahBawah == 1) {
                    //lamalif dengan alif berhamzah bawah
                    return "\u0644\u0625";
                } else {
                    return "\u0644\u0627";
                }
            case "AA0":
                //angka 0, atau yang salah mirip
                if (nTitikBawah == 1 && nTitikAtas == 0) {
                    //ba'
                    return "\u0628";
                } else if (nTitikBawah == 2 && nTitikAtas == 0) {
                    //ya
                    return "\u064a";
                } else if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //nun
                    return "\u0646";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //ta
                    return "\u062a";
                } else if (nTitikBawah == 0 && nTitikAtas == 3) {
                    //tsa
                    return "\u062b";
                }
                //angka 0 yang beneran
                return "\u0660";
            case "AA1":
                //angka 1
                return "\u0661";
            case "AA2":
                //angka 2
                return "\u0662";
            case "AA3":
                //angka 3
                return "\u0663";
            case "AA4":
                //angka 4
                return "\u0664";
            case "AA5":
                //angka 5
                if (nTitikBawah == 0 && nTitikAtas != 0) {
                    //ta marbutah, setidaknya ada titik satu
                    return "\u0629";
                }
                return "\u0665";
            case "AA6":
                //angka 6
                return "\u0666";
            case "AA7":
                //angka 7
                return "\u0667";
            case "AA8":
                //angka 8
                return "\u0668";
            case "AA9":
                //tunggal-awal-tengah-akhir: fa, qaf
                if (nTitikBawah == 0 && nTitikAtas == 1) {
                    //fa, titiknya satu
                    return "\u0641";
                } else if (nTitikBawah == 0 && nTitikAtas == 2) {
                    //qaf, titiknya dua
                    return "\u0642";
                } 
                //angka 9
                return "\u0669";
        }

        return "";
    }

    public static void main(String[] args) {
        BinaryImageShell bin = new BinaryImageShell("_raw/mainbody/01/110.png");

        Classifier cl = new Classifier();
        cl.loadDataDefault();
        Instances dataUnlabeled = new Instances("Rel", cl.getFeatureVector(), 0);
        Instance in = cl.extractFeature(bin);
        dataUnlabeled.add(in);
        String code = cl.classify(dataUnlabeled.firstInstance());
    }
}
