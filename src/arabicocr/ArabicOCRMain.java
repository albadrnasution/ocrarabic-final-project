/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package arabicocr;

import window.DocImageUI;

/**
 * Hanya menjalankan window utama yang harus dijalankan.
 * @author Albadr
 */
public class ArabicOCRMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new DocImageUI().setVisible(true);
            }
        });
    }
}
